$(document).ready(function(){
  var controller = new ScrollMagic.Controller();

  
  $('.conocimientos').each(function(){
          var ourScene = new ScrollMagic.Scene({
              triggerElement: this.children[0],
              triggerHook: 0.9
          })
          .setClassToggle(this, 'fade-in') // add class to project01
          // this requires a plugin
          .addTo(controller);

      });


      // define movement of panels
		var wipeAnimation = new TimelineMax()
			// animate to second panel

			.to("#slideContainer", 1,   {x: "-20%"})	// move in to first panel
			.to("#slideContainer", 1, {z: 0})				// move back to origin in 3D space
			// animate to third panel
			.to("#slideContainer", 1,   {x: "-40%"})
			.to("#slideContainer", 1, {z: 0})
			// animate to forth panel
			.to("#slideContainer", 1,   {x: "-60%"})
            .to("#slideContainer", 1, {z: 0})

			.to("#slideContainer", 1,   {x: "-80%"})
            .to("#slideContainer",1, {z: 0})

        

		// create scene to pin and link animation
		new ScrollMagic.Scene({
				triggerElement: "#pinContainer",
				triggerHook: "onLeave",
				duration: "500%"
			})
			.setPin("#pinContainer")
			.setTween(wipeAnimation)
			.addTo(controller);

});
